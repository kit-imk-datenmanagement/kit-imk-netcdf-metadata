﻿# Measurement and Observation Specific Global Attributes

## Instrument Global Attributes
:::{table}
:width: 80%
:widths: grid
:name: instrument-global-attributes
| attribute | status | description | format | vocabulary | example |
| --------- | ------ | ----------- | ------ | -------- | ------- |
| device_name | O | name of the device or sensor | free text | IMK-RDM |PINE-05-02, EM27 |
| device_id | O | ID or PID of the device or sensor e.g. DOI, handle | free text | IMK-RDM| |
| platform_name | O | name of the measurement platform | free text | IMK-RDM | e.g. KITcube, AIDA, Halo |
| platform_id | O | ID or PID of the measurement platform e.g. DOI, handle | free text | IMK-RDM | |
:::

## Measurement Location Global Attributes
:::{table}
:width: 80%
:widths: grid
:name: measurement-global-attributes
| attribute | status | description | format | vocabulary | example |
| --------- | ------ | ----------- | ------ | -------- | ------- |
|geographic_name | O | name of region, province or city the measurements were conducted | free text | Hereon | e.g. Paris, Black Forest |
| station_name | O | name of the station the measurements were conducted | free text | | Sonnblick Observatory |
| station_id | O | ID or PID of the station the measurements were conducted e.g from GAW, ACTRIS, Tereno | infrastructure: ID | | GAW: SNB |
:::