*Global attributes* are the netCDF file’s metadata. Only information that applies to the file as a whole is to be included here.

A distinction is made between *mandatory attributes* (Status: M) and *optional attributes* (Status: O). The optional attributes are necessary to accommodate the different data sources and disciplines.

Attribute names can generally be freely chosen, but there are attribute names that are already reserved in the CF standard and can only be used for specified purposes. The latest standard version 1.11 is used for CF conventions. The CF conventions are the base level conventions for all other established standards, which e.g. extended the attributes. Therefore, only the CF standard is mentioned in the vocabulary, even if the other standards also list this attribute.


# General Global Attributes on the Data

:::{table}
:width: 80%
:widths: grid
:name: general-global-attributes
| attribute | status | description | format | vocabulary | example |
| --------- | ------ | ----------- | ------ | -------- | ------- |
| comment | O | short description of the file contents; essentially like an abstract | free text | CF| ozone data assembled from models and field observations |
| contact | M | Contact information for the dataset: Person, email or url to get in touch. For CMIP comma-separated list like name, institute, email | free text | CF | Eva Musterfrau </br> for CMIP "John Doe, john\@example.com" |
| contact_email | O | If mandatory attribute *contact* was filled with a name, the corresponding email adress can be given here. |email adress | Hereon | eva.musterfrau\@kit.edu |
| Conventions | M | Convention version | free text | CF |  CF-1.11, CMIP6.2 |
| creation_date | M | creation date of the file | timestamp ISO 8601:</br> YYYY-MM-DDTHH:MM:SSZ | Hereon, ATMODAT, CMIP | 2023-12-12T11:32:00, 2010-03-23T05:56:23+01:00 |
| creator_name | M | Creator/ author of the file. | free text | ACDD | Max Mustermann |
| creator_email | M | email adress of the creator/author of the file. | email adress | ACDD | max.mustermann\@kit.edu |
| creator_url | M | Website url or ORCID of the creator/ author of the file. </br>Don't have an ORCID yet? Register [here](https://orcid.org/register) for free. | URL | ACDD | https\://orcid.org/0000-0000-0000-0000 |
| distribution_statement | O | coordinated text on scope of the data, acknowledgement guidelines,... | free text | Hereon|  We had help from...; Data is available for use of registered users... |
| further_info_url | O (M for CMIP) | URL with detailed information about the project, measurement or experiment e.g. document server, website. </br>For CMIP: Location of documentation| URL | CMIP | https\://www.atmohub.kit.edu/english/590.php, https\://wcrp-cmip.github.io/WGCM_Infrastructure_Panel/Papers/CMIP6_global_attributes_filenames_CVs_v6.2.7.pdf |
| history | M | List of the applications that have modified the original data. This should be a kind of logbook with timestamp and file treatment note. | YYYY-MM-DD: description of modification| CF | *cdo does this automatically*</br> 2024-01-10: python program xy published on git (link) was used |
| institution | M | long name of the submitting institution | free text | CF | Karlsruhe Institute of Technology, Institute of Meteorology and Climate Research, Germany |
| institution_id | O (M for CMIP)| identifier of the submitting institution e.g [ROR](https://ror.org/>) (Research Organization Registry)</br>for CMIP standard abbreviation of the institution | ID-type: ID </br>free text| CMIP | ROR:04t3en479</br>for CMIP: KIT|
| keywords | O | comma-separated list of keywords or phrases preferably from a standard vocabulary | free text | ACDD | e.g. qbo, stratosphere, wind, satellite, model, icon |
| keywords_vocabulary | O | standard/ vocabulary name for the given keywords | free text | ACDD | e.g. GCMD, NERC, CF |
| license | O (M for CMIP) | short name of the license assigned to the submitted data</br>for CMIP: license terms governing the use of the dataset | free text | ACDD | e.g. CC-BY-SA 4.0</br> for CMIP: more info [here](#add-info-license) |
| processing_level | O | Comma-separated list of processing steps representing the transformation, manipulation or analyses that were applied to the source to produce the final data product. E.g. processing chain, processing level information, QA/QC. | free text |  ACDD |e.g. raw data, duplicates removed, errors removed, drift correction applied 
| processing_software | O | comma-separated list with name and version, or repository URL of the software | free text | Hereon | PIA 2.0.2 |
| references | O | Published or web-based references that describe the data or methods used to produce it like URL, DOI | free text, URL recommended | CF | https\://doi.org/10.5194/amt-14-1143-2021 |
| publication_url | O | URL like DOI of the published data set | URL | IMK RDM | |
| source | M | The production method of the original data. In case it was model-generated, source should name the model and its version, as specifically as could be useful. For  observational data, source should characterize it (e.g., surface observation or radiosonde). | free text | CF | e.g. surface observation or world model v0.1 |
| title | M | brief description of the data | free text | CF| e.g. Radiosonde data from 12th June, 2019 at 09:54 in Dittersdorf as part of the MOSES project, CMIP6 Historical Simulation |
:::

(add-info-license)=
## Additional information to license
CMIP requests the following: 
“CMIP6 model data produced by *<Your Centre Name>* is licensed under a Creative Commons Attribution-[+]ShareAlike 4.0 International License (https://creativecommons.org/licenses/). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file)[and at <some URL maintained by modeling group>].  The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.”
The [+] indicates that institutions may choose to use the Non-commercial version of this license by inserting the words “NonCommercial-” at this point, but this will significantly limit the use of the data in downstream climate mitigation and adaptation applications.  Please do not simply copy the statement above when writing data; Some text must be entered, some text is optional and the symbols “[+]” should not appear in the licensing text.

# Time Dimension Global Attributes
:::{table}
:width: 80%
:widths: grid
:name: timedimension-global-attributes
| attribute | status | description | format | vocabulary | example |
| --------- | ------ | ----------- | ------ | -------- | ------- |
| time_coverage_duration | O | e.g. length of the data sets | number unit </br> ISO 8601 | ACDD| 1d </br> P1D |
| time_coverage_resolution | O | e.g. spacing of the timestamps in this file | number unit </br> | ACDD | 1s </br> PT1S|
| time_coverage_start | O | start time of the measurements in this file | timestamp ISO 8601 | ACDD | 2023-12-12T13:18:00 |
| time_coverage_end | O | end time of the measurements in this file | timestamp ISO 8601 | ACDD | 2023-12-12T13:18:00 |
:::

# Geospatial Coordinate Global Attributes
:::{table}
:width: 80%
:widths: grid
:name: geospatial-global-attributes
| attribute | status | description | format | vocabulary | example |
| --------- | ------ | ----------- | ------ | -------- | ------- |
| featureType | O | for discrete sampling geometry see [CF convention](https://cfconventions.org/cf-conventions/cf-conventions.html#_features_and_feature_types) | free text | CF | e.g. point, timeSeries, trajectory, profile |
| crs | O | coordinate reference system, default WGS84 | EPSG: code | ATMODAT| ESPG:4326 |
| geospatial_lat_resolution | O | e.g. spacing of points in the latitude | number unit | ACDD| 2 degree |
| geospatial_lon_resolution | O | e.g. spacing of points in the longitude | number unit | ACDD | 2 degree |
| geospatial_vertical_resolution | O | e.g. spacing of points in the altitude | number unit | ACDD | 1 meters |
| geospatial_lat_min | O | Minimum latitude in the data | number unit | ACDD | -23.2 |
| geospatial_lat_max | O | Maximum latitude in the data | number unit | ACDD | 84.7985 |
| geospatial_lat_units | O | Unit for the latitude axis boundaries described in *geospatial_lat_min*/ *geospatial_lat_max* or *start_latitude*/ *stop_latitude* | free text | ACDD | degrees_north |
| geospatial_lon_min | O | Minimum longitude in the data | number unit | ACDD | -154.7154 |
| geospatial_lon_max | O | Maximum longitude in the data | number unit | ACDD | 30.36 |
| geospatial_lon_units | O | Unit for the longitude axis boundaries described in *geospatial_lon_min* and *geospatial_lon_max* or *start_longitude*/ *stop_longitude* | free text | ACDD | degrees_east |
| geospatial_vertical_min | O | Minimum altitude in the data | number unit | ACDD | 2 |
| geospatial_vertical_max | O | Maximum altitude in the data | number unit | ACDD | 2386.9 |
| geospatial_vertical_units | O | Unit for the vertical axis boundaries described in *geospatial_vertical_min* and *geospatial_vertical_max* | free text | ACDD | m or EPSG:5829 for height above sea level |
| geospatial_vertical_positive | O | direction of the vertical dimension<br /> If "up", vertical values are interpreted as 'altitude', with negative values corresponding to below the reference datum/level (e.g., under water).| | ACDD | up |
| start_latitude | O | start latitude of the measurements (from a cruise or flight) in this file | number unit | Hereon | 53.2 |
| stop_latitude | O | end latitude of the measurements (from a cruise or flight) in this file | number unit | Hereon | 53.2 |
| start_longitude | O | start longitude of the measurements (from a cruise or flight) in this file | number unit | Hereon | -122.5 |
| stop_longitude | O | end longitude of the measurements (from a cruise or flight) in this file | number unit | Hereon | 80.4 |
:::

# Project Related Global attributes
:::{table}
:width: 80%
:widths: grid
:name: project-global-attributes
| attribute | status | description | format | vocabulary | example |
| --------- | ------ | ----------- | ------ | -------- | ------- |
| contributor_name | O | comma-separated list of contributors | first name name | ISO 19115 | Max Mustermann, John D. Smith, Jane C. Doe | 
| contributor_role | O | comma-separated list of contributors roles | first name name | ISO 19115 | co-author, technician, data manager
| PI_name | O | comma-separated list of the campaign/ experiment PI. | first name name | IMK RDM| Max Mustermann |
| PI_email | O | comma-separated list of email adress(es) of the campaign/ experiment PI(s) | | IMK RDM | max.mustermann\@kit.edu |
| PI_url | O | comma-separated list of Website URL(s) or ORCID(s) of the campaign/ experiment PI(s)<br/> Don't have an ORCID yet? Register [here](https://orcid.org/register) for free. | | IMK RDM | https\://orcid.org/0000-0000-0000-0000 |
| operator_name | O | comma-separated list of campaign/ experiment operators. | first name name | IMK RDM | Max Mustermann |
| operator_email | O | comma-separated list of email adress(es) of the campaign/ experiment operator(s) | | IMK RDM | max.mustermann\@kit.edu |
| operator_url | O | comma-separated list of Website URL(s) or ORCID(s) of the campaign/ experiment operator(s)<br/> Don't have an ORCID yet? Register [here](https://orcid.org/register) for free. | | IMK RDM | https\://orcid.org/0000-0000-0000-0000 |
| program | O | name of the superordinate infrastructure, project, funding, ... | free text | ACDD | e.g. NDACC, MOSES |
| project | O | name of the project within the program or campaign | free text | ACDD | Swabian MOSES 2023 |
| experiment_id | O | experiment identifier | free text | Hereon |2023_MOSES_Swabian_IOP01 |
:::
