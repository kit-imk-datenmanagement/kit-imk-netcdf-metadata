﻿# Data Types
In NetCDF files only the following data types are allowed:
:::{table}
:width: 80%
:widths: grid
| type | description | Python3 numpy equivalent type | [NetCDF atomic external types](https://docs.unidata.ucar.edu/nug/current/md_types.html#data_type) |
| ---- | ----------- | ------------- | ---------------------------- |
| byte | 8-bit signed integer | numpy.byte/numpy.int8 | NC_BYTE |
| unsigned byte | 8-bit unsigned integer | numpy.ubyte/numpy.uint8 | NC_UBYTE |
| short | 16-bit signed integer | numpy.short/numpy.int16 | NC_SHORT |
| unsigned short | 16-bit unsigned integer | numpy.ushort/ numpy.uint16 | NC_USHORT |
| int | 32-bit signed integer | numpy.intc/numpy.int32 | NC_INT |
| unsigned int | 32-bit unsigned integer | numpy.uintc/numpy.uint32 | NC_INT |
| int64 | 64-bit signed integer | numpy.int_/numpy.int64 | NC_INT64 |
| unsigned int64 | 64-bit unsigned integer | numpy.uint/numpy.uint64 | NC_UINT64 |
| float or real | single precision (32-bit) floating point number | numpy.single/numpy.float32 | NC_FLOAT |
| double | double precision (64-bit) floating point number | numpy.double/np.float64 | NC_DOUBLE |
| string | | | NC_STRING |
| char | | | NC_CHAR |
:::

:::{note}
* signed means a data interval from -a to (a-1) depending on the programming language and unsigned means only positive values.
* string variables can be defined in two ways:
    1. atomic string *NC_STRING*
    2. character array
:::
::::{admonition} Example
:class: tip
Example for Note 2.b. string variable:
Assuming you want to put the name of the months, then you can define the variable as
1. string type with 12 dimensions ('January', 'February',...,'December')<br/>
or as
2. char type with (12,9) dimensions ((NULL,NULL,'J','a','n,'u','a','r','y'),(NULL,'F','e','b','r','u','a','r','y'),...,(NULL,'D','e','c','e','m','b','e','r'))
:::{code-block}
dimensions:
	strings = 12 ;
	strlen = 9 ; (length of the longest string)
variables:
	string months_str(strings);
		months_str:long_name="name of months" ;
	char months_char(strings,strlen);
		months_char:long_name="name of months" ;
:::
::::

**User defined types** allow for more complex data structures. However, CF is not recommending the use of such types. NetCDF-4 has added support for four different [user defined types](https://docs.unidata.ucar.edu/netcdf-c/current/group__user__types.html#details).
* compound type: like a C struct, a compound type is a collection of types, including other user defined types, in one package.
* variable length array type: used to store ragged arrays.
* opaque type: This type has only a size per element, and no other type information.
* enum type: Like an enumeration in C, this type lets you assign text values to integer values, and store the integer values.