﻿# Naming Conventions
* Variables, dimensions and attribute names begin with a letter and consist of letters, numbers and underscores.
* Spaces, umlauts, ß, special characters (except underscore “_”) are not allowed.
* Upper and lower case letters are significant in netCDF names, but it is recommended that names should not only be distinguished by case, i.e., if case is disregarded, no two names should be the same. 

Variable names should adhere as much as possible to international standards. For example, for atmospheric quantities, the [CF standard names table](https://cfconventions.org/Data/cf-standard-names/current/build/cf-standard-name-table.html) is available.

Refer to the [Guidelines for Construction of CF Standard Names](http://cfconventions.org/Data/cf-standard-names/docs/guidelines.html) for information on how the names are constructed. The guidelines may help to interpret the names and to suggest how new names could be derived. Please send requests and questions concerning standard names to the [IMK RDM team](mailto:core-rdm-imk@lists.kit.edu?subject=CF%20standardnames).

:::{admonition} Example
:class: tip
*    pressure, longitude, air_pressure_at_sea_level, mole_concentration_of_water_vapor_in_air are *valid* variable names
*    geogr. Breite, air pressure, flughöhe, $velocity are all *invalid* variable names.
*    Variable names such as PRESSURE and pressure are not to be used together in the same netCDF file because they can only be differentiated in terms of case sensitivity. This also applies to dimensions and attribute names.
:::

(file-names)=
# File Name Conventions
* NetCDF files are denoted with the extension .nc
* The file name starts with a letter or a letter sequence. The letter sequence should indicate the type of data contained in the file (e.g. instrument, project).
* File names should not contain spaces, umlauts, “ß”, or special characters, except for underscore “_”, dash “-”, and periods “.”. 
* For model data, either the model name with the version designation or the *experiment_id* should be used in the first segment of the filename.
* File names are encoded with the starting date (and possibly the end date) of the data contained in the file according to the format: YYYYMMDD. The date provided complies with the scope of data. A yearly file usually only contains the numerical year listed as YYYY. If appropriate, hours  can be added in the format "hh", minutes in “mm” and seconds in “ss”. Unresolved timescales can be omitted. Additional information can be added after the date and after “_” or “.”, such as variable designations, region, statistical processing abbreviations.

:::{admonition} Example
:class: tip
* CARIBIC_20010610_HOG_DUS_V04.nc for a CARIBIC measurements on June 10th, 2010 during a flight from airport HOG to DUS, data set version 04.
* ICON-ART-Tsurfmean_idealizedSimulation_20210612_0900-1800.nc for an ICON ART idealized simulation for the 12th June 2021 from 9 to 12 UTC. 
* ERA5_DT20230101_00.nc for ERA5 data for potential temperature on 1st January 2023 at 00 UTC.
* Radiosonde_Moses_Dittersdorf_20190612_0954.nc for radiosonde data from 12th June, 2019 at 09:54 in Dittersdorf as part of the MOSES project.
:::

# Other Conventions
* Some attributes in the following tables allow multiple entries as comma-separated lists. Any entries within such a list which contain a comma must be enclosed in straight double quotation marks.
:::{admonition} Example
:class: tip
List of names:
Max Mustermann, Jane Lee, "John Smith, Jr."
:::