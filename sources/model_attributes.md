﻿# Model Specific Global Attributes

For the model specific global attributes we following the [controlled vocabulary ](https://docs.google.com/document/d/1h0r8RZr_f3-8egBMMh7aqLwy3snpD6_MrDz1q8n5XUk) from the [World Climate Research Programme (WCRP) Coupled Model Intercomparison Project (CMIP)](https://www.wcrp-climate.org/wgcm-cmip).
The [Earth System Grid Foundation (ESGF)](https://esgf.llnl.gov/) created their own [variable definition tables](https://github.com/PCMDI/cmip6-cmor-tables/tree/master/Tables) for the CMIP output and this is often used by other projects. There are some conditional mandatory fields that are indicated by "(M)".

:::{table}
:width: 80%
:widths: grid
:name: model-global-attributes
| attribute | status | description | format | vocabulary | example |
|-------------------------|--------|--------------------------------------------------|--------------------------|------------|----------------|
| activity_id | M | space-separated list of activity identifier(s) from a controlled vocabulary | free text | CMIP | CMIP |
| branch_method |(M)| Branching procedure                           | free form | CMIP | "perturbations to atmospheric fields were applied at the branch time, followed by a 50 year spin-up period under control conditions" |
| branch_time_in_child |(M)| Branch time with respect to child’s time axis | double precision float | CMIP| 0.0                                           |
| branch_time_in_parent|(M)| Branch time with respect to parent time axis  | double precision float | CMIP| 3650.0                                        |
| data_specs_version   | M | Version identifier                            | string    | CMIP | "cmip6_data_specs_version 01.00.31"           |
| experiment           | M | Short experiment description                  | string    | CMIP | “pre-industrial control”,  “abrupt quadrupling of CO2”                        |
| experiment_id        | M | root experiment identifier                    | string    | CMIP | “historical”, “abrupt4xCO2”                       |
| external_variables   |(M)| External cell measures                        | string    | CMIP | "areacello volcello"                          |
| forcing_index        | M | Index for variant of forcing                  | integer >0| CMIP | 1, 2, 82, 323                                             |
| frequency            | M | Sampling frequency                            | string    | CMIP | “mon”, “day”, “6hr”                              |
| grid                 | M | Grid                                          | free form | CMIP | "Native atmosphere T63 gaussian grid (128x64 lonxlat)" |
| grid_label           | M | Grid identifier                               | string    | CMIP | “gn”, “gr”, “gr1”, “gr2”, “grz”, “gm”                           |
| initialization_index | M | Index for variant of initialization method    | integer >0| CMIP | 1                                             |
| mip_era              | M | Activity’s associated CMIP cycle            | string         | CMIP | “CMIP5”, “CMIP6”                                    |
| nominal_resolution   | M | Approximate horizontal resolution           | string         | CMIP | “50 km”, “100 km”, “250 km”,  “1x1 degree”              |
| parent_activity_id   |(M)| Parent activity identifier                  | string         | CMIP | "CMIP6 AerChemMIP"                                         |
| parent_experiment_id |(M)| Parent experiment identifier                | string         | CMIP | "piControl"                                         |
| parent_mip_era       |(M)| Parent’s associated MIP cycle               | string         | CMIP | "CMIP6"                                             |
| parent_source_id     |(M)| Parent model identifier                     | string         | CMIP | "CESM2"                                             |
| parent_time_units    |(M)| Time units used in parent                   | structured form| CMIP | “days since 1850-1-1”, “days since 1000-1-1 (noleap)”     |
| parent_variant_label |(M)| Parent variant label                        | modified form  | CMIP | “r1i1p1f1”, “r1i2p223f3”, “no parent”               |
| physics_index        | M | Index for model physics variant             | integer >0 | CMIP | 1                                                             |
| realization_index    | M | Index distinguishing among ensemble members | integer >0 | CMIP | 1                                                             |
| realm                | M | realm(s) where variable is defined      | string  | CMIP | “atmos”, “ocean”, “atmosChem atmos”                                   |
| source               | M | Model and version                       | string  | CMIP | "CESM2.0 (2018): atmos: CAM (cam9, T85L96); ocean: POP (pop2.0, gx1v7); seaIce: CICE (cice5.1.2); land: CLM (clm5.0)" |
| source_id            | M | Modified source identifier              | string  | CMIP | "CESM2-0"                                                             |
| source_type          | M | Model components used in the experiment | string  | CMIP | “AGCM”, “OGCM”,  “AOGCM”, “ISM”, “AOGCM ISM”                         |
| sub_experiment       | M | description of sub-experiment           | string  | CMIP | “tas”, “pr”, “ua”                                                     |
| sub_experiment_id    | M | sub-experiment identifier               | string  | CMIP | “s1960”, “s1965”,  “none”                                              |
| table_id             | M | table identifier                        | string  | CMIP | “Amon”, “Oday”                                                       |
| tracking_id          | M | Unique identifier for the file          | string  | CMIP | "hdl:21.14100/02d9e6d5-9467-382e-8f9b-9300a64ac3cd" |
| variable_id           | M | variable identifier                     | string  | CMIP | “tas”, “pr”, “ua”                                                     |
| variant_info         |O| recommended, description of the run variant          |free form| CMIP | “forcing: black carbon aerosol only”                                |
| variant_label        | M | Label of the model variant              | string  | CMIP | “r1i1p1f1”, “f1i2p223f3”                                        |
:::