﻿# Metadata guidelines for environmental data

*Sabine Barthlott [<img src="https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png">](https://orcid.org/0000-0003-0258-9421) \
Nicole Büttner \
Benjamin Ertl \
Romy Fösig [<img src="https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png">](https://orcid.org/0000-0002-8162-4354) \
Sibylle Haßler [<img src="https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png">](https://orcid.org/0000-0001-5411-8491) \
Tobias Kerzenmacher [<img src="https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png">](https://orcid.org/0000-0001-8413-0539") \
Katharina Loewe [<img src="https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png">](https://orcid.org/0000-0003-3574-7203") \
Christof Lorenz [<img src="https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png">](https://orcid.org/0000-0001-5590-5470) \
Corinna Rebmann [<img src="https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png">](https://orcid.org/0000-0002-8665-0375)*

:::{rubric} Contact
:::
[IMK RDM team](mailto:core-rdm-imk@lists.kit.edu?subject=metadata%20guidelines)

This document is based on \
Breitbach G., Geyer B., Kleeberg U., Meyer E., Onken R., Sommer P.S., Binding Regulations for Storing Data as netCDF Files, URL: <https://gitlab.hzdr.de/hcdc/hereon-netcdf/hereon-netcdf-en>, Release 1.6 2021-10-15

## Introduction

IMK institute files should follow a uniform netCDF format for seamless exchange and dissemination. The following guidelines govern the naming of both attributes and variables. Additionally, specifications outline which attributes should be associated with certain variables. While not all specifications are mandatory, adjustments may be required in certain cases.

This information essentially refers to the agreements within the netCDF [Climate and Forecast (CF)](https://cfconventions.org/index.html) Metadata Conventions (hereafter referred to as the CF Conventions), the [Attribute Convention for Data Discovery (ACDD)](https://wiki.esipfed.org/Attribute_Convention_for_Data_Discovery_1-3#contributor_name), the [Atmospheric Model Data (AtMoDat) standard](https://doi.org/10.35095/WDCC/atmodat_standard_en_v3_0) and the [World climate Research Programme (WCRP) Coupled Model Intercomparison Project (CMIP)](https://www.wcrp-climate.org/wgcm-cmip). The relevant recommendations are summarised and expanded in the following document. This document will be revised when necessary.

There are also other conventions in projects or communities. If a different standard is prescribed in your project, please use this. 

After saving the data according to the specified regulations, the files should be checked using e.g. the [CF Conventions Compliance Checker for NetCDF Format](https://cfchecker.ncas.ac.uk/).

After successful checks, data are ready for dissemination and publication in open access repositories (e.g. [RADAR4KIT](https://radar.kit.edu/radar/en/home), [KITopen](https://dbkit.bibliothek.kit.edu/catalog), [PANGAEA](https://www.pangaea.de/), [Zenodo](https://zenodo.org)) and could be harvested by [Cat4KIT](https://cat4kit.atmohub.kit.edu/?.language=en).

:::{note}
Note, that all examples in the document are minimal examples to better explain and clarify a certain attribute or specification. This means that the variables or the file itself may not be fully described. 
:::

:::{toctree}
:hidden:
:caption: General Specifications
general_specifications.md
:::

:::{toctree}
:hidden:
:caption: Global Attributes
global_attributes.md
measurement_attributes.md
model_attributes.md
:::

:::{toctree}
:hidden:
:caption: Variable Specifications
data_types.md
variable_attributes.md
:::

:::{toctree}
:hidden:
:caption: Examples
example_stationary_ts.md
:::
