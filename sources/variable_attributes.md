# Variable Attributes

See Chapter [File Names](#file-names) for variable naming conventions.
:::{table}
:width: 80%
:widths: grid
:name: variable-attributes
| attribute | status | description | format | vocabulary | example |
| --------- | ------ | ----------- | ------ | -------- | ------- |
| calendar | M | only for time coordinate, see Section [Coordinates](#coordinates) for more details | | CF | e.g. proleptic_gregorian, standard |
| flag_mask | M | only for ancillary variable *_qc*, see Section [Quality Flags](#quality-flags) for more details | | CF | |
| flag_meanings | M | only for ancillary variable *_qc*, see Section [Quality Flags](#quality-flags) for more details | | CF | |
| flag_values | M | only for ancillary variable *_qc*, see Section [Quality Flags](#quality-flags) for more details | | CF | |
| long_name | M | descriptive name which may, for example, be used for labeling plots | free text | CF | longitude |
| positive | M | only for vertical coordinate, see Section [Coordinates](#coordinates) for more details | | CF | up |
| standard_name | O | standard name of the variable following a defined vocabulary | | CF | air_temperature |
| standard_name_vocab | O | vocabulary for the chosen variable standard name | | ACDD | e.g. CF, NERC |
| units | M | unit (SI unit is strongly recommended) of the variable | | CF | e.g. m or 1 for dimensionless units |
| ancillary_variables | O | linking one varibale to another e.g. by providing metadata | | CF | See Section [Ancillary Variables](#ancillary-vars) for more details. |
| axis | O | only for geospatial coordinates, see Section [Coordinates](#coordinates) for more details | | CF | X |
| bounds | O | see Section [Time Bounds](#time-bounds) for more details | | CF | |
| cell_methods | O | Indicates that for an axis identified by name, the cell values representing the field have been determined or derived by the specified method e.g. aggregation like mean. see Section [Time Bounds](#time-bounds) for more details | name: method | CF | time: mean |
| comment | O | comment if needed | free text | CF | |
| coordinates | O | space-separated list of auxiliary variables, if projections other than WGS84 are used | list of dimensions | CF | lat lon height_10m |
| \_FillValue | O | Value that indicates undefined or missing data. Attribute *missing_value* is deprecated. NaN is not recommended. See Section [Fill Values](#fill-value) for more details. | same type as its variable | CF | -9999.99 |
| grid_mapping | O | Indicates the projection used. See list of standard [grid_mappings](https://cfconventions.org/Data/cf-conventions/cf-conventions-1.9/cf-conventions.html#appendix-grid-mappings) | | CF | earth_radius |
| valid_range | O | smallest and largest valid values of a variable | comma separated list | CF | 0, 100 |
| valid_min | O | smallest valid value of a variable | number | CF | 0 |
| valid_max | O | largest valid value of a variable | number | CF | 100 |
:::

::::{admonition} Example
:class: tip

:::{code-block}
dimensions:
    time=UNLIMITED;
    rlon=64;
    rlat=69
variables:
    double time(time);
        time:standard_name="time";
        time:long_name="time";
        time:units="seconds since 1979-01-01 00:00:00";
        time:calendar="proleptic_gregorian";
    float rlon(rlon);
        rlon:standard_name="grid_longitude";
        rlon:long_name="rotated longitude";
        rlon:units="degrees";
    float rlat(rlat);
        rlat:standard_name="grid_latitude";
        rlat:long_name="rotated latitude";
        rlat:units="degrees";
    float height_2m;
        height_2m:standard_name="height";
        height_2m:long_name="height above the surface";
        height_2m:units="m";
        height_2m:positive="up";
    float T_2m(time, rlon, rlat);
        T_2m:standard_name="air_temperature";
        T_2m:long_name="2m temperature";
        T_2m:units="K";
        T_2m:valid_range="253.15, 303.15";
        T_2m:coordinates="lon lat height_2m";
        T_2m:cell_methods = "time:mean (interval: 1 month)" ;
:::
::::

(coordinates)=
## Coordinates
For the **geospatial latitude and longitude** variables it is strongly recommended to use the following attribute values:
:::{table}
:width: 80%
:widths: grid
| name | unit | standard_name | axis (optional) |
| ---- | ---- | ------------- | --------------- |
| lat | degrees_north, <br>only degrees when using transformed grids| latitude | Y |
| lon | degrees_east, <br>only degrees when using transformed grids | longitude | X |
:::

For the **vertical coordinates** unit and direction (positive) must be specified. Units for dimensional vertical coordinates can be
* units of pressure e.g. hPa or
* units of length e.g. m

For dimensionless vertical coordinates unit is not mandatory.
::::{admonition} Example subsurface measurements
:class: tip
An oceanographic netCDF file encodes the depth of the surface as 0 and the depth of 1000 meters as 1000 then the axis would use attributes as follows:
:::{code-block}
axis_name:units = "m" ;
axis_name:positive = "down" ;
:::
If, on the other hand, the depth of 1000 meters were represented as -1000 then the value of the positive attribute would have been "up".
::::

::::{note}
If the units attribute value is a valid pressure unit the default value of the positive attribute is down. For pressure in the atmosphere one could simply use 
:::{code-block}
axis_name:units = "hPa" ;
:::
::::

The unit for the **time coordinate** is `"days since YYYY-MM-DD HH:mm:SS"` or any other suitable formulation. It is strongly recommended to give the time coordinate in UTC. For the time coordinate the attribute *calendar* must be set to *proleptic_gregorian* or *standard*.

(ancillary-vars)=
## Ancillary Variables
The attribute *ancillary_variables* is used to express relationships between variables, e.g. uncertainties, quality flags, detection limits or other depending variables. The attribute is given as a blank-separated list of the standard names of the linked variables. The linked variable itself must be given like any other variable.
::::{admonition} Example
:class: tip
Measurement of the cloud droplet effective radius with the associated variable *_status* indicating if the measurement was conducted within a (liquid) cloud e.g LWC>0.5 g m-3 means status 1.
:::{code-block}
float r_eff(time);
    r_eff:standard_name="effective_radius_of_cloud_liquid_water_particles";
    r_eff:long_name="cloud droplet effective radius";
    r_eff:units="m";
    r_eff:ancillary_variables="r_eff_status r_eff_qc"
int r_eff_status(time);
    r_eff_status:long_name="status of cloud occurence"
    r_eff_status:units="1";
    r_eff_status:valid_range="0, 1";
:::
How to use the _qc varibale will be explained in Section [Quality Flags](#quality-flags).
::::

(fill-value)=
## Fill Values or Missing Values
Missing values in the data are indicated by the *\_FillValue* attribute, which is of the same type as its variable. The attribute can be set to NaN, but this can lead to problems in some visualization tools. More detailed information can be found in the [CF conventions for missing data](https://cfconventions.org/Data/cf-conventions/cf-conventions-1.11/cf-conventions.html#missing-data).

(time-bounds)=
## Time Bounds
For aggregated data it is necessary to include bounds over which the data was aggregated. E.g. average data to a daily mean could mean averaging from noon to noon or from midnight to midnight or any other time. Furthermore, the given timestamp must not be the mean time, but also the start or end of the aggregation interval.
::::{admonition} Example
:class: tip
Horizontal wind component *u* averaged over one month.
1. The aggregation of the variable *u* is indicated by the attribute *cell_methods*. Since it is a temporal average, in the *cell_methods* attribute *time:mean* must be given. Additionally in braces, the interval is given by 1 month. This is not mandatory, but recommended.
2. A new variable *time_bnds* is defined indicating the time interval bounds for averaging. This variable is also related to the time variable in the attribute *bounds*. The time_bnds variable is defined as a tuple consisting of the timestep value and the following time step (t, t+1 month). The tuple dimension is given by *nv*.
:::{code-block}
dimensions:
	time = 853 ;
	pressure = 15 ;
	string13 = 13 ;
	nv = 2 ;
variables:
	double u(time, pressure) ;
		u:_FillValue = NaN ;
		u:standard_name = "eastward_wind" ;
		u:long_name = "Eastward Wind" ;
		u:units = "m/s" ;
		u:cell_methods = "time: mean (interval: 1 month)" ;
		u:u_qc = 0LL ;
	int time_bnds(time, nv) ;
		time_bnds:long_name = "time bounds" ;
	int time(time) ;
		time:standard_name = "time" ;
		time:long_name = "Time" ;
		time:units = "days since 1950-01-01 00:00:00" ;
		time:calendar = "standard" ;
		time:bounds = "time_bnds" ;
	int nv(nv) ;
		nv:long_name = "Number of Vertices in Bounds" ;
		nv:units = "count" ;
:::
::::

(quality-flags)=
## Quality Flags

If possible, data should always be denoted with quality flags. For observation data, quality flags are mandatory. For each data point, these quality flags indicate whether the data point has undergone quality control (or not) and the specific result of this quality control. The quality flag does not provide any information whether the data point is close to reality or how high its uncertainty is.

In the netCDF file, the quality flag is an additional variable that has the same name as the variable for which the quality specification applies, but with an appended string “\_qc” (following the CF conventions). “qc” stands for quality control for the respective variable. It is strongly recommended to relate this qc variable to the main variable using the *ancillary_variables* attribute. The qc variables have the same dimensions as their associated main variable.
According to the CF convention, there are 2 ways to describe the qc variable: *flag_values* or *flag_masks*. Below, two examples show how to use them. The *flag_meanings* attribute is used in both cases to provide descriptive words or phrases for each assigned flag like a translation list.
    
The way quality flag values are specified may depend on the program/ project the measurements were conducted in. Therefore, the following example shows only one possible way.
::::{admonition} Example
:class: tip
GAW-WDCA [PM10 mass concentration data set](https://ebas-data.nilu.no/Pages/DataSetList.aspx?key=2794887F3D864EDE96A1B19FEA76F144) for 2022 measured at Schneefernerhaus/Zugspitze.
The data is flagged with (simplified for this example):
* 0 valid
* 1 data below detection limit (blue data points)
* 3 for aggregation data completness below limit (orange data points)
* 4 Saharan dust event (green data points)
* 9 missing
    
![](images/example_pm10_Zugspitze_actris_2022.png)

:::{code-block}
starttime    ...    PM10    flag_PM10
2022-01-01 00:00:00    ...    2.676	0
2022-01-01 01:00:00    ...    2.776	0
2022-01-01 01:59:59    ...    2.996	0
...
2022-02-11 19:59:59    ...    1.774	0
2022-02-11 21:00:00    ...    1.104	0
2022-02-11 22:00:00    ...    1.025	3
2022-02-11 22:59:59    ...    1.014	0
2022-02-12 00:00:00    ...    0.902	0
2022-02-12 01:00:00    ...    0.64	0
2022-02-12 01:59:59    ...    0.509	0
2022-02-12 03:00:00    ...    0.485	0
2022-02-12 04:00:00    ...    0.409	1
2022-02-12 04:59:59    ...    0.411	1
2022-02-12 06:00:00    ...    0.429	1
2022-02-12 07:00:00    ...    0.517	0
2022-02-12 07:59:59    ...    0.57	0
...
2022-03-18 10:00:00    ...    9999.999	9
2022-03-18 10:59:59    ...    9999.999	9
2022-03-18 12:00:00    ...    25.113	3
2022-03-18 13:00:00    ...    28.144	4
2022-03-18 13:59:59    ...    31.684	4
2022-03-18 15:00:00    ...    28.88	4
2022-03-18 16:00:00    ...    24.04	4
...
:::

:::{code-block}
float pm10_mass(time);
    pm10_mass:standard_name="mass_concentration_of_pm10_ambient_aerosol_in_air";
    pm10_mass:standard_name_vocab="CF";
    pm10_mass:long_name="PM10 mass concentration";
    pm10_mass:units="µg m-3";
    pm10_mass:_FillValue="9999.999";
    pm10_mass:ancillary_variables="pm10_mass_qc";
int64 pm10_mass_qc(time);
    pm10_mass_qc:standard_name="";
    pm10_mass_qc:standard_name_vocab="";
    pm10_mass_qc:long_name = "quality flag for PM10 mass concentration";
    pm10_mass_qc:flag_values = 0, 1, 3, 4, 9;
    pm10_mass_qc:flag_meanings = "valid invalid_below_DL valid_SDE valid_data_completness invalid_missing";
:::
::::
