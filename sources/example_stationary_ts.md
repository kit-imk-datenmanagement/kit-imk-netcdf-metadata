﻿# stationary timeseries data

Data taken over periods of time at a discrete sampling point e.g. a measurement station is called a timeseries data set.
::::{admonition} Example
:class: tip
Assuming a data set at one single sampling point with strict monotonically increasing time stamps. *time* would therefore be the only one-dimensional coordinate. In order to locate the data, station is given as an additional dimension here, e.g. number of stations. In the following, the GAW-WDCA [PM10 mass concentration data set](https://ebas-data.nilu.no/Pages/DataSetList.aspx?key=2794887F3D864EDE96A1B19FEA76F144) for 2022 measured at Schneefernerhaus/Zugspitze is  described.
:::{code-block}
dimensions:
    station = 1 ; //no of measurement locations
    time = 8760 ;
variables:
    double time(time) ;
        time:standard_name = "time" ;
        time:standard_name_vocab = "CF" ;
        time:long_name = "time of measurement" ;
        time:units = "days since 2022-01-01" ;
        time:calendar = "proleptic_gregorian" ;
    string station_name(station) ;
        station_name:standard_name = "platform_name" ;
        station_name:standard_name_vocab = "CF" ;
        station_name:long_name = "station name" ;
        station_name:units = "1" ;
        station_name:cf_role = "timeseries_id" ;
    float lon ;
        lon:standard_name = "longitude" ;
        lon:standard_name_vocab = "CF" ;
        lon:long_name = "station longitude" ;
        lon:units = "degrees_east" ;
        lon:axis = "X" ;
    float lat ;
        lat:standard_name = "latitude" ;
        lat:standard_name_vocab = "CF" ;
        lat:long_name = "station latitude" ;
        lat:units = "degrees_north" ;
        lat:axis = "Y" ;
    float alt ;
        alt:standard_name = "altitude" ;
        alt:standard_name_vocab = "CF" ;
        alt:long_name = "station altitude" ;
        alt:units = "m" ;
        alt:axis = "Z" ;
        alt:positive = "up" ;
    float meas_height ;
    	meas_height:standard_name = "height_above_mean_sea_level" ;
    	meas_height:standard_name_vocab: "CF" :
    	meas_height:long_name = "height of the pm10 inlet" ;
    	meas_height:units = "m" ;
    	meas_height:axis = "Z" ;
        meas_height:positive = "up" ;
    float pm10_mass(time) ;
        pm10_mass:standard_name = "mass_concentration_of_pm10_ambient_aerosol_particles_in_air" ;
        pm10_mass:standard_name_vocab = "CF" ;
        pm10_mass:long_name = "PM10 mass concentration" ;
        pm10_mass:units="µg m-3" ;
        pm10_mass:_FillValue="9999.999" ;
        pm10_mass:valid_range = "-2.233, 419.998" ;
        pm10_mass:coordinates = "time lat lon alt meas_height station_name" ;
        pm10_mass:cell_methods = "time:mean" ;
        pm10_mass:ancillary_variables = "pm10_mass_qc" ;
    float pm10_mass_qc(time);
        pm10_mass_qc:standard_name = "" ;
        pm10_mass_qc:standard_name_vocab = "" ;
        pm10_mass_qc:long_name = "quality flag for PM10 mass concentration";
        pm10_mass_qc:units = "1" ;
        pm10_mass_qc:flag_values = "" ;
        pm10_mass_qc:flag_meanings = "" ;
attributes:
    :featureType = "timeSeries" ;
    :contact = "Cedric Couret" ;
    :contact_mail = "cedric.couret@uba.de" ;
    :Conventions = "CF-1.11" ;
    :creation_date = "2023-03-16" ;
    :creator_name = "Cedric Couret" ;
    :creator_mail = "cedric.couret@uba.de" ;
    :creator_url = "https://orcid.org/0000-0002-1874-9883" ;
    :history = "" ;
    :institution = "German Environment Agency, UBA, II.4.5 GAW Globalobservatorium Zugspitze/Hohenpeissenberg Platform Zugspitze" ;
    :institution_id = "ROR:0329ynx05" ;
    :license = "CC BY 4.0" ;
    :processing_level = "ACTRIS level 2" ;
    :source = "surface observation" ;
    :title = "pm10_mass - beta_gauge_particulate_sampler at Zugspitze-Schneefernerhaus" ;
    :time_coverage_duration = "P365D" ;
    :time_coverage_resolution = "P1D" ;
    :time_coverage_start = "2022-01-01" ;
    :time_coverage_end = "2022-12-31" ;
    :program = "GAW-WDCA" ;
    :device_name = "ThermoScientific_SHARP_C14_pm10" ;
    :station_name = "Zugspitze-Schneefernerhaus" ;
    :station_id= "GAW:ZSF" ;
:::
::::