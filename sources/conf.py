# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Metadata guidelines for environmental data'
copyright = '2024, KIT IMK RDM team'
release = '1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ["myst_parser", "sphinx_design", 'sphinx_simplepdf']
myst_enable_extensions = ['colon_fence', "html_admonition"]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

source_suffix = ['.rst', '.md']  # {'.rst': 'restructuredtext', '.md': 'markdown'}


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme' #'alabaster'
html_static_path = ['_static']
html_css_files = ['mytheme.css']
html_logo = '_static/KITlogo.png'
html_theme_options = {
		'logo_only': True,
		'style_nav_header_background': 'white',		
		'sticky_navigation': True,
		'navigation_depth': 3
		}
